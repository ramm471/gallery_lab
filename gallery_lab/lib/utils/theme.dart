import 'package:flutter/material.dart';

class AppTheme {
  static get theme {
    final originalTextTheme = ThemeData.dark().textTheme;
    final originalBody = originalTextTheme.bodyText1;

    return ThemeData.dark().copyWith(
        primaryColor: Colors.grey[300],
        primaryColorDark: Colors.grey[500],
        backgroundColor: Colors.grey[800],
        textTheme: originalTextTheme.copyWith(bodyText1: originalBody?.copyWith(decorationColor: Colors.transparent)),
        colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Colors.green[500]));
  }
}
