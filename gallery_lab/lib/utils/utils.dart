Map<T, List<S>> groupBy<S, T>(Iterable<S> values, T Function(S) key) {
  var map = <T, List<S>>{};
  for (var element in values) {
    (map[key(element)] ??= []).add(element);
  }
  return map;
}

T? safeCast<T>(dynamic obj) {
  try {
    return (obj as T);
  } on TypeError catch (e) {
    print('CastError when trying to cast $obj to $T!');
    return null;
  }
}
