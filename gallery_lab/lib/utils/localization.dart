import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

//TODO add localization
class ImagesLocalizations {
  static ImagesLocalizations? of(BuildContext context) {
    return Localizations.of<ImagesLocalizations>(context, ImagesLocalizations);
  }

  String get appTitle => "Gallery Lab";

  String get addImage => "Add Image";

  String get map => "Map";

  String get images => "Images";

  String get imageDetails => "Image Details";

  String get newImageUrlHint => "Choose an image";

  String get emptyImageUrlError => "Please choose an image";

  String get emptyImageNoteError => "Please enter some words";

  String get notesHint => "Image description";

  String get selectLocation => "Select Location";

  String get imageMap => "Image map";
}

class ImagesLocalizationsDelegate extends LocalizationsDelegate<ImagesLocalizations> {
  @override
  Future<ImagesLocalizations> load(Locale locale) => Future(() => ImagesLocalizations());

  @override
  bool shouldReload(ImagesLocalizationsDelegate old) => false;

  @override
  bool isSupported(Locale locale) => locale.languageCode.toLowerCase().contains("en");
}
