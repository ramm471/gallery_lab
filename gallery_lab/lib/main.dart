import 'package:bloc/bloc.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gallery_lab/images_app_widget.dart';
import 'package:gallery_lab/images_bloc/images_bloc.dart';
import 'package:gallery_lab/images_bloc/images_event.dart';
import 'package:gallery_lab/repository/image_storage_helper.dart';
import 'package:gallery_lab/repository/images_repository.dart';
import 'package:gallery_lab/utils/bloc_observer.dart';
import 'package:provider/provider.dart';

import 'const/google_const.dart';
import 'const/keys.dart';
import 'map_bloc/map_cubit.dart';
import 'map_bloc/map_markers_helper.dart';

Future<void> main() async {
  Bloc.observer = SimpleBlocObserver();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MultiProvider(
    providers: [
      Provider<DatabaseReference>(
        create: (context) => FirebaseDatabase.instance.reference().child(GoogleConst.firebaseImagesPath),
      ),
      Provider<Reference>(
        create: (context) => FirebaseStorage.instance.ref(),
      ),
      Provider<MapMarkerHelper>(
        create: (context) => MapMarkerHelperImpl(),
      ),
      Provider<ImagesRepository>(
        create: (context) => ImagesRepositoryImpl(context.read<DatabaseReference>()),
      ),
      Provider<ImageStorageHelper>(
        create: (context) => ImageStorageHelperImpl(context.read<Reference>()),
      ),
    ],
    child: MultiBlocProvider(
      providers: [
        BlocProvider<ImagesBloc>(
          create: (context) => ImagesBloc(
            imagesRepository: context.read<ImagesRepository>(),
          )..add(ImagesLoaded()),
        ),
        BlocProvider<MapCubit>(
          create: (context) => MapCubit(
            BlocProvider.of<ImagesBloc>(context),
            context.read<MapMarkerHelper>(),
          ),
        ),
      ],
      child: const ImagesApp(
        key: ImagesKeys.appKey,
      ),
    ),
  ));
}
