import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:gallery_lab/tab_bloc/tab_event.dart';

import 'app_tab.dart';

class TabBloc extends Bloc<TabEvent, AppTab> {
  TabBloc() : super(AppTab.images);

  @override
  Stream<AppTab> mapEventToState(TabEvent event) async* {
    if (event is TabUpdated) {
      yield event.tab;
    }
  }
}
