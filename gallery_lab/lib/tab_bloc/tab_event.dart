import 'package:equatable/equatable.dart';
import 'package:gallery_lab/tab_bloc/app_tab.dart';

abstract class TabEvent extends Equatable {
  const TabEvent();
}

class TabUpdated extends TabEvent {
  final AppTab tab;

  const TabUpdated(this.tab);

  @override
  List<Object> get props => [tab];

  @override
  String toString() => 'TabUpdated { tab_bloc: $tab }';
}
