import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gallery_lab/repository/image_storage_helper.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'add_marker_state.dart';

class AddMarkerCubit extends Cubit<AddMarkerState> {
  final ImageStorageHelper _imageStorageHelper;

  AddMarkerCubit(this._imageStorageHelper) : super(AddMarkerScreenInitialState());

  Future<void> loadImage(String path) async {
    emit(state.copyWith(isLoading: true));
    var url = await _imageStorageHelper.uploadImageToFirebase(File(path));
    if (url != null) {
      emit(state.copyWith(images: [url, ...state.images], isLoading: false));
    } else {
      emit(state.copyWith(isLoading: false));
    }
  }

  Future<void> addLocation(LatLng coordinates) async {
    emit(state.copyWith(coordinates: coordinates));
  }
}
