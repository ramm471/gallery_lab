import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class AddMarkerState extends Equatable {
  final List<String> images;
  final LatLng? coordinates;
  final String? note;
  final bool? isLoading;

  const AddMarkerState(this.images, this.coordinates, this.note, this.isLoading);

  @override
  List<Object?> get props => [images, coordinates, note, isLoading];

  AddMarkerState copyWith({List<String>? images, LatLng? coordinates, String? note, bool? isLoading}) {
    return AddMarkerState(
      images ?? this.images,
      coordinates ?? this.coordinates,
      note ?? this.note,
      isLoading ?? this.isLoading,
    );
  }
}

class AddMarkerScreenInitialState extends AddMarkerState {
  AddMarkerScreenInitialState() : super([], null, null, false);
}
