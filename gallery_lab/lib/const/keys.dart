import 'package:flutter/widgets.dart';

class ImagesKeys {
  static const appKey = Key('__images_app__');
  static const homeScreen = Key('__home_screen__');
  static const addImageScreen = Key('__add_image_screen__');
  static const imagesTab = Key('__images_tab__');
  static const mapTab = Key('__map_tab__');
  static const addImageFab = Key('__add_image_fab__');
  static const tabs = Key('__tabs__');
  static const mapScreen = Key('__map_screen__');
  static const imagesLoading = Key('__images_loading__');
  static const imageList = Key('__image_list__');
  static const imageListHorizontal = Key('__image_list_horizontal__');
  static const imagesEmptyContainer = Key('__images_empty_container__');
  static const imageDetailsScreen = Key('__image_details_screen__');
  static const urlField = Key('__url_field__');
  static const noteField = Key('__note_field__');
  static const saveNew = Key('__save_new__');
  static const uploadImageField = Key('__upload_image_field__');
  static const pickImage = Key('__pick_image__');
  static const googleMapPicker = Key('__google_map_picker__');
  static const imageMapScreen = Key('__image_map_screen__');
  static const imagePickerButton = Key('__image_picker_button__');
  static const mapWithMarkers = Key('__map_with_markers__');

  static imageItem(String id) => Key('image_item__${id}__');

  static filteredImagesScreen(String id) => Key('filtered_images_screen__${id}__');

  static filteredImages(String id) => Key('filtered_images__${id}__');
}
