import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoogleConst {
  static const CameraPosition initialLocation = CameraPosition(
    target: LatLng(53.893009, 27.567444),
    zoom: 14.4746,
  );

  static const String firebaseImagesPath = "images";
  static const double defaultZoom = 14.4746;
  static const double defaultMarkerDevicePixelRatio = 2.5;
}
