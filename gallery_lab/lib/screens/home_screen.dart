import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gallery_lab/const/keys.dart';
import 'package:gallery_lab/tab_bloc/app_tab.dart';
import 'package:gallery_lab/widjets/image_list.dart';
import 'package:gallery_lab/tab_bloc/tab_bloc.dart';
import 'package:gallery_lab/tab_bloc/tab_event.dart';
import 'package:gallery_lab/utils/localization.dart';
import 'package:gallery_lab/utils/routes.dart';
import 'package:gallery_lab/widjets/map_widget.dart';
import 'package:gallery_lab/widjets/tab_selector.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<TabBloc, AppTab>(
      builder: (context, activeTab) {
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            title: Text(ImagesLocalizations.of(context)?.appTitle ?? ""),
            actions: const [],
          ),
          body: activeTab == AppTab.images
              ? const ImagesList(key: ImagesKeys.imagesTab)
              : const MapWidget(key: ImagesKeys.mapTab),
          floatingActionButton: FloatingActionButton(
            key: ImagesKeys.addImageFab,
            onPressed: () {
              Navigator.pushNamed(context, ImagesRoutes.addImage);
            },
            child: const Icon(Icons.add),
            tooltip: ImagesLocalizations.of(context)?.addImage ?? "",
          ),
          bottomNavigationBar: TabSelector(
            activeTab: activeTab,
            onTabSelected: (tab) => BlocProvider.of<TabBloc>(context).add(TabUpdated(tab)),
            key: ImagesKeys.tabs,
          ),
        );
      },
    );
  }
}
