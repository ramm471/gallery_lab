import 'dart:async';

import 'package:flutter/material.dart';
import 'package:gallery_lab/const/google_const.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

typedef OnSelectCallback = Function(LatLng latLng);

class GoogleMapPickerScreen extends StatefulWidget {
  final OnSelectCallback onSelectCallback;

  const GoogleMapPickerScreen({Key? key, required this.onSelectCallback}) : super(key: key);

  @override
  State<GoogleMapPickerScreen> createState() => GoogleMapPickerScreenState();
}

class GoogleMapPickerScreenState extends State<GoogleMapPickerScreen> {
  final Completer<GoogleMapController> _controller = Completer();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GoogleMap(
        onTap: _mapTapped,
        mapType: MapType.hybrid,
        initialCameraPosition: GoogleConst.initialLocation,
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
      ),
    );
  }

  _mapTapped(LatLng location) {
    print(location);
    widget.onSelectCallback(location);
    Navigator.pop(context);
  }
}
