import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gallery_lab/const/keys.dart';
import 'package:gallery_lab/images_bloc/images_bloc.dart';
import 'package:gallery_lab/images_bloc/images_state.dart';
import 'package:gallery_lab/utils/localization.dart';
import 'package:photo_view/photo_view.dart';

class DetailsScreen extends StatelessWidget {
  final String id;

  const DetailsScreen({Key? key, required this.id}) : super(key: key ?? ImagesKeys.imageDetailsScreen);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ImagesBloc, ImagesState>(
      builder: (context, state) {
        final image = (state as ImagesLoadSuccess).images.firstWhere((image) => image.id == id);
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            title: Text(ImagesLocalizations.of(context)?.imageDetails ?? ""),
          ),
          body: Stack(children: <Widget>[
            Center(
              child: PhotoView(
                imageProvider: NetworkImage(image.url),
              ),
            ),
            Positioned.fill(
              child: Align(
                child: Text(image.note),
                alignment: Alignment.bottomCenter,
              ),
              bottom: 20,
            ),
          ]),
        );
      },
    );
  }
}
