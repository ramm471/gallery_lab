import 'package:flutter/material.dart';
import 'package:gallery_lab/const/keys.dart';
import 'package:gallery_lab/images_bloc/image.dart' as app_image;
import 'package:gallery_lab/utils/localization.dart';
import 'package:gallery_lab/widjets/image_list.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class FilteredImagesScreen extends StatelessWidget {
  final app_image.Image image;

  const FilteredImagesScreen({Key? key, required this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(ImagesLocalizations.of(context)?.appTitle ?? ""),
      ),
      body: ImagesList(
        key: ImagesKeys.filteredImages(image.id),
        initialCoordinates: LatLng(image.latitude, image.longitude),
      ),
    );
  }
}
