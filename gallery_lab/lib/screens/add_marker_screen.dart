import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gallery_lab/add_marker_block/add_marker_cubit.dart';
import 'package:gallery_lab/add_marker_block/add_marker_state.dart';
import 'package:gallery_lab/const/keys.dart';
import 'package:gallery_lab/utils/localization.dart';
import 'package:gallery_lab/widjets/upload_image_widget.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'google_map_picker.dart';

typedef OnSaveCallback = Function(List<String> urls, String note, LatLng location);

class AddMarkerScreen extends StatefulWidget {
  final OnSaveCallback onSaveCallback;
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  const AddMarkerScreen({Key? key, required this.onSaveCallback}) : super(key: key);

  @override
  State<AddMarkerScreen> createState() => _AddMarkerScreenState();
}

class _AddMarkerScreenState extends State<AddMarkerScreen> {
  String? _note;

  @override
  Widget build(BuildContext context) {
    final AddMarkerCubit block = BlocProvider.of<AddMarkerCubit>(context);

    return BlocBuilder<AddMarkerCubit, AddMarkerState>(
      builder: (context, state) {
        final List<String> urls = state.images;
        print("urls $urls");
        return Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.black,
            title: Text(ImagesLocalizations.of(context)?.appTitle ?? ""),
            actions: const [],
          ),
          body: Padding(
            padding: const EdgeInsets.all(_screenPadding),
            child: Form(
              key: AddMarkerScreen._formKey,
              child: Column(
                children: [
                  Row(
                    children: [
                      ImagePickerButton(key: ImagesKeys.imagePickerButton),
                      SizedBox(
                        height: 150,
                        width: (MediaQuery.of(context).size.width - addButtonSize * 2 - _screenPadding * 2),
                        child: ListView.builder(
                          key: ImagesKeys.imageListHorizontal,
                          itemCount: urls.length,
                          scrollDirection: Axis.horizontal,
                          addAutomaticKeepAlives: true,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.all(8),
                              child: CachedNetworkImage(
                                imageUrl: urls[index],
                                width: MediaQuery.of(context).size.width / 3,
                                height: MediaQuery.of(context).size.width / 3,
                                fit: BoxFit.fitWidth,
                                errorWidget: (context, url, error) => const Icon(Icons.error),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => GoogleMapPickerScreen(
                            key: ImagesKeys.googleMapPicker,
                            onSelectCallback: (location) {
                              block.addLocation(location);
                            },
                          ),
                        ),
                      );
                    },
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: _getSelectLocationText(state.coordinates),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(6, 0, 6, 0),
                    child: TextFormField(
                      key: ImagesKeys.noteField,
                      decoration: InputDecoration(hintText: ImagesLocalizations.of(context)?.notesHint),
                      validator: (val) {
                        return val?.trim().isEmpty ?? false
                            ? ImagesLocalizations.of(context)?.emptyImageNoteError
                            : null;
                      },
                      onSaved: (value) {
                        setState(() {
                          _note = value;
                        });
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            key: ImagesKeys.saveNew,
            tooltip: ImagesLocalizations.of(context)?.addImage,
            child: const Icon(Icons.save),
            onPressed: () {
              var selectedUrls = state.images;
              var selectedLocation = state.coordinates;
              if (selectedLocation != null &&
                  selectedUrls.isNotEmpty &&
                  (AddMarkerScreen._formKey.currentState?.validate() ?? false)) {
                AddMarkerScreen._formKey.currentState?.save();
                widget.onSaveCallback(selectedUrls, _note ?? "", selectedLocation);
                Navigator.pop(context);
              }
            },
          ),
        );
      },
    );
  }
}

const double _screenPadding = 16;

Text _getSelectLocationText(LatLng? coordinates) {
  var text =
      coordinates != null ? "${coordinates.latitude} - ${coordinates.longitude}" : ImagesLocalizations().selectLocation;
  return Text(
    text,
    textAlign: TextAlign.start,
    style: TextStyle(
      color: Colors.grey.withOpacity(1.0),
      fontSize: 16,
    ),
  );
}
