import 'dart:async';
import 'dart:core';
import 'dart:io';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart';

abstract class ImageStorageHelper {
  Future<String?> uploadImageToFirebase(File file);
}

class ImageStorageHelperImpl implements ImageStorageHelper {
  final Reference _firebaseStorageRef;

  ImageStorageHelperImpl(
    this._firebaseStorageRef,
  );

  @override
  Future<String?> uploadImageToFirebase(File file) async {
    var taskSnapshot = await _firebaseStorageRef.child('uploads/${basename(file.path)}').putFile(file);
    var url = taskSnapshot.ref.getDownloadURL();
    print("uploadImageToFirebase url: $url");
    return url;
  }
}
