class ImageEntity {
  final String id;
  final String note;
  final String url;
  final double latitude;
  final double longitude;

  ImageEntity(this.id, this.note, this.url, this.latitude, this.longitude);

  @override
  int get hashCode => id.hashCode ^ note.hashCode ^ url.hashCode ^ latitude.hashCode ^ longitude.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ImageEntity &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          note == other.note &&
          url == other.url &&
          latitude == other.latitude &&
          longitude == other.longitude;

  Map<String, Object> toJson() {
    return {
      "id": id,
      "note": note,
      "url": url,
      "latitude": latitude,
      "longitude": longitude,
    };
  }

  @override
  String toString() {
    return 'ImageEntity{id: $id, note: $note, url: $url, latitude: $latitude, longitude: $longitude}';
  }

  static ImageEntity fromJson(Map<String, Object> json) {
    return ImageEntity(
      json["id"] as String,
      json["note"] as String,
      json["url"] as String,
      parseCoordinates(json["latitude"]),
      parseCoordinates(json["longitude"]),
    );
  }

  static double parseCoordinates(Object? value) {
    if (value is int) {
      return value.toDouble();
    } else {
      return value as double;
    }
  }
}
