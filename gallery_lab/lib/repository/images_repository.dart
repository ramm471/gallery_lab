import 'dart:async';
import 'dart:core';

import 'package:firebase_database/firebase_database.dart';
import 'package:gallery_lab/repository/image_entity.dart';

abstract class ImagesRepository {
  Future<List<ImageEntity>> loadImages();

  Future saveImage(List<ImageEntity> images);
}

class ImagesRepositoryImpl implements ImagesRepository {
  final DatabaseReference _imagesRef;

  ImagesRepositoryImpl(this._imagesRef);

  @override
  Future<List<ImageEntity>> loadImages() async {
    print("loadImages");
    DataSnapshot snapshot = await _imagesRef.once();
    List<ImageEntity> entities = [];
    if (snapshot.value == null) return entities;

    for (var entity in snapshot.value.entries) {
      print("loadImage $entity");
      try {
        ImageEntity parsedEntity = ImageEntity.fromJson(Map<String, Object>.from(entity.value));
        print("loadImage parsedEntity $parsedEntity");
        if (!entities.contains(parsedEntity)) {
          entities.add(parsedEntity);
        }
      } catch (e) {
        print("loadImage $e");
      }
    }
    return entities;
  }

  @override
  Future saveImage(List<ImageEntity> images) async {
    print("saveImage $images");
    for (var image in images) {
      _imagesRef
          .push()
          .set(image.toJson())
          .whenComplete(() => print("savedImage $image"))
          .onError((error, stackTrace) => print("saveImages $error"));
    }
  }
}
