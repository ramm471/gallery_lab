import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gallery_lab/const/keys.dart';
import 'package:gallery_lab/images_bloc/image.dart' as app_image;
import 'package:gallery_lab/images_bloc/images_bloc.dart';
import 'package:gallery_lab/images_bloc/images_event.dart';
import 'package:gallery_lab/repository/image_storage_helper.dart';
import 'package:gallery_lab/screens/add_marker_screen.dart';
import 'package:gallery_lab/tab_bloc/tab_bloc.dart';
import 'package:gallery_lab/utils/localization.dart';
import 'package:gallery_lab/utils/routes.dart';
import 'package:gallery_lab/utils/theme.dart';

import 'add_marker_block/add_marker_cubit.dart';
import 'screens/home_screen.dart';

class ImagesApp extends StatelessWidget {
  const ImagesApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: ImagesLocalizations().appTitle,
      theme: AppTheme.theme,
      localizationsDelegates: [
        ImagesLocalizationsDelegate(),
      ],
      routes: {
        ImagesRoutes.home: (context) {
          return BlocProvider<TabBloc>(
            create: (context) => TabBloc(),
            child: const HomeScreen(
              key: ImagesKeys.homeScreen,
            ),
          );
        },
        ImagesRoutes.addImage: (context) {
          return BlocProvider<AddMarkerCubit>(
            create: (context) => AddMarkerCubit(context.read<ImageStorageHelper>()),
            child: AddMarkerScreen(
              key: ImagesKeys.addImageScreen,
              onSaveCallback: (urls, note, latLng) {
                List<app_image.Image> images = urls
                    .map((url) => app_image.Image(
                          url: url,
                          note: note,
                          latitude: latLng.latitude,
                          longitude: latLng.longitude,
                        ))
                    .toList();
                BlocProvider.of<ImagesBloc>(context).add(ImageAdded(images));
              },
            ),
          );
        },
      },
    );
  }
}
