import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:gallery_lab/const/google_const.dart';
import 'package:gallery_lab/images_bloc/image.dart' as app_image;
import 'package:gallery_lab/utils/utils.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

typedef OnMarkerTap = Function(app_image.Image image);

abstract class MapMarkerHelper {
  Future<CameraPosition> extractInitialPosition(List<app_image.Image> images);

  Future<Set<Marker>> getMarkers(List<app_image.Image> images, OnMarkerTap onTap);
}

class MapMarkerHelperImpl extends MapMarkerHelper {
  @override
  Future<CameraPosition> extractInitialPosition(List<app_image.Image> images) async {
    if (images.isNotEmpty) {
      return CameraPosition(
        target: LatLng(images.first.latitude, images.first.longitude),
        zoom: GoogleConst.defaultZoom,
      );
    } else {
      return GoogleConst.initialLocation;
    }
  }

  @override
  Future<Set<Marker>> getMarkers(List<app_image.Image> images, OnMarkerTap onTap) async {
    Map<LatLng, List<app_image.Image>> grouped = groupBy(images, (image) => LatLng(image.latitude, image.longitude));
    final markers = <Marker>{};
    for (var latLng in grouped.keys) {
      var marker = await _mapToMarker(latLng, grouped[latLng], onTap);
      if (marker is Marker) {
        markers.add(marker);
      }
    }
    print("<<<<<<MapCubit>>>>>>> markers ${markers.map((e) => e.markerId)}");
    return markers;
  }

  Future<Marker?> _mapToMarker(LatLng latLng, List<app_image.Image>? images, OnMarkerTap onTap) async {
    if (images != null && images.isNotEmpty) {
      //TODO replace with image icon
      var icon = await _getDefaultMarker();
      return Marker(
        markerId: MarkerId(images.first.id),
        position: latLng,
        icon: icon,
        //TODO replace with custom
        infoWindow: InfoWindow(
            title: "Photos: ${images.length}",
            onTap: () {
              onTap(images.first);
            }),
      );
    } else {
      return null;
    }
  }

  Future<BitmapDescriptor> _getDefaultMarker() => BitmapDescriptor.fromAssetImage(
        const ImageConfiguration(devicePixelRatio: GoogleConst.defaultMarkerDevicePixelRatio),
        'assets/location_icon.png',
      );
}
