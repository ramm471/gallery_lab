import 'package:equatable/equatable.dart';
import 'package:gallery_lab/const/google_const.dart';
import 'package:gallery_lab/images_bloc/image.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapState extends Equatable {
  final Set<Marker> markers;
  final CameraPosition initialLocation;
  final Image? selectedImage;

  const MapState(this.markers, this.initialLocation, this.selectedImage);

  @override
  List<Object?> get props => [markers, initialLocation, selectedImage];

  MapState copyWith({Set<Marker>? markers, CameraPosition? initialLocation, Image? selectedMarker}) {
    return MapState(
      markers ?? this.markers,
      initialLocation ?? this.initialLocation,
      selectedMarker,
    );
  }
}

class MapScreenInitialState extends MapState {
  MapScreenInitialState() : super(<Marker>{}, GoogleConst.initialLocation, null);
}
