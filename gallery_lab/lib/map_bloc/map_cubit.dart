import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gallery_lab/images_bloc/image.dart' as app_image;
import 'package:gallery_lab/images_bloc/images_bloc.dart';
import 'package:gallery_lab/images_bloc/images_state.dart';

import 'map_markers_helper.dart';
import 'map_state.dart';

class MapCubit extends Cubit<MapState> {
  final ImagesBloc imagesBloc;
  final MapMarkerHelper markerHelper;
  StreamSubscription<ImagesState>? _imagesSubscription;

  MapCubit(this.imagesBloc, this.markerHelper) : super(MapScreenInitialState()) {
    print("<<<<<<MapCubit>>>>>>>");
    _imagesSubscription = _subscribeToImages();
  }

  @override
  Future<void> close() {
    print("<<<<<<MapCubit>>>>>>> close");
    _imagesSubscription?.cancel();
    return super.close();
  }

  StreamSubscription<ImagesState> _subscribeToImages() {
    print("<<<<<<MapCubit>>>>>>> monitorImagesState");
    _update(imagesBloc.state);
    return imagesBloc.stream.listen((event) {
      print("_update runtime $event");
      _update(event);
    });
  }

  void _update(ImagesState event) async {
    print("<<<<<<MapCubit>>>>>>> _update $event");
    if (event is ImagesLoadSuccess) {
      var initialState = await markerHelper.extractInitialPosition(event.images);
      var markers = await markerHelper.getMarkers(
        event.images,
        _onTap,
      );
      emit(state.copyWith(markers: markers, initialLocation: initialState));
    }
  }

  _onTap(app_image.Image image) {
    print("<<<<<<MapCubit>>>>>>> _onTap selectedMarker ${image.id}");
    emit(state.copyWith(selectedMarker: image));
    emit(state.copyWith(selectedMarker: null));
  }
}
