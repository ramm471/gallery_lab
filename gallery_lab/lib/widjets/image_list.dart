import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gallery_lab/const/keys.dart';
import 'package:gallery_lab/images_bloc/images_bloc.dart';
import 'package:gallery_lab/images_bloc/images_state.dart';
import 'package:gallery_lab/screens/details_screen.dart';
import 'package:gallery_lab/widjets/image_item.dart';
import 'package:gallery_lab/widjets/loading_indicator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ImagesList extends StatelessWidget {
  final LatLng? initialCoordinates;

  const ImagesList({required Key key, this.initialCoordinates}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ImagesBloc, ImagesState>(
      builder: (context, state) {
        if (state is ImagesLoadInProgress) {
          return const LoadingIndicator(key: ImagesKeys.imagesLoading);
        } else if (state is ImagesLoadSuccess) {
          final images = state.images.where((element) {
            if (initialCoordinates == null) {
              return true;
            } else {
              return (element.longitude == initialCoordinates?.longitude &&
                  element.latitude == initialCoordinates?.latitude);
            }
          }).toList();
          print("ImagesLoadSuccess $images");
          return ListView.builder(
            key: ImagesKeys.imageList,
            itemCount: images.length,
            addAutomaticKeepAlives: true,
            itemBuilder: (BuildContext context, int index) {
              final image = images[index];
              return ImageItem(
                  key: ImagesKeys.imageItem(image.id),
                  image: image,
                  onTap: () async {
                    Navigator.of(context).push(
                      MaterialPageRoute(builder: (_) {
                        return DetailsScreen(id: image.id);
                      }),
                    );
                  });
            },
          );
        } else {
          return Container(key: ImagesKeys.imagesEmptyContainer);
        }
      },
    );
  }
}
