import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:gallery_lab/const/keys.dart';
import 'package:gallery_lab/tab_bloc/app_tab.dart';
import 'package:gallery_lab/utils/localization.dart';

class TabSelector extends StatelessWidget {
  final AppTab activeTab;
  final Function(AppTab) onTabSelected;

  const TabSelector({
    required Key key,
    required this.activeTab,
    required this.onTabSelected,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      key: ImagesKeys.tabs,
      currentIndex: AppTab.values.indexOf(activeTab),
      onTap: (index) => onTabSelected(AppTab.values[index]),
      backgroundColor: Colors.black,
      items: AppTab.values.map((tab) {
        return BottomNavigationBarItem(
          icon: Icon(
            tab == AppTab.images ? Icons.list : Icons.map,
            key: tab == AppTab.images ? ImagesKeys.imagesTab : ImagesKeys.mapTab,
          ),
          label: tab == AppTab.map ? ImagesLocalizations.of(context)?.map : ImagesLocalizations.of(context)?.images,
        );
      }).toList(),
    );
  }
}
