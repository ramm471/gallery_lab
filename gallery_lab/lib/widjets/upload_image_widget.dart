import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gallery_lab/add_marker_block/add_marker_cubit.dart';
import 'package:gallery_lab/add_marker_block/add_marker_state.dart';
import 'package:image_picker/image_picker.dart';

class ImagePickerButton extends StatelessWidget {
  final _picker = ImagePicker();

  ImagePickerButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AddMarkerCubit, AddMarkerState>(
      builder: (context, state) {
        print("isLoading ${state.isLoading}");
        return (state.isLoading == true)
            ? const SizedBox(
                width: addButtonSize * 2,
                child: Center(
                  child: CircularProgressIndicator(
                    color: Colors.green,
                  ),
                ),
              )
            : SizedBox(
                width: addButtonSize * 2,
                child: TextButton(
                  child: Icon(
                    Icons.add_a_photo,
                    size: addButtonSize,
                    color: Colors.green[500],
                  ),
                  onPressed: () {
                    _pickImage(context);
                  },
                ),
              );
      },
    );
  }

  Future _pickImage(BuildContext context) async {
    final pickedFile = await _picker.pickImage(
      source: ImageSource.camera,
      maxWidth: MediaQuery.of(context).size.width,
      imageQuality: 50,
    );
    if (pickedFile != null) {
      BlocProvider.of<AddMarkerCubit>(context).loadImage(pickedFile.path);
    }
  }
}

const double addButtonSize = 50;
