import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:gallery_lab/const/keys.dart';
import 'package:gallery_lab/map_bloc/map_cubit.dart';
import 'package:gallery_lab/map_bloc/map_state.dart';
import 'package:gallery_lab/screens/filtered_images_screen.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapWidget extends StatelessWidget {
  const MapWidget({Key? key}) : super(key: key ?? ImagesKeys.imageMapScreen);

  @override
  Widget build(BuildContext context) {
    final Completer<GoogleMapController> _controller = Completer();
    return Scaffold(
      body: BlocConsumer<MapCubit, MapState>(
        listener: (context, state) {
          print("_handleMarkerTap $state");
          var selectedImage = state.selectedImage;
          if (selectedImage != null && selectedImage.id.isNotEmpty) {
            print("_handleMarkerTap navigate to ${selectedImage.id}");
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => FilteredImagesScreen(
                    key: ImagesKeys.filteredImagesScreen(selectedImage.id),
                    image: selectedImage,
                  ),
                ));
          }
        },
        builder: (context, state) {
          return GoogleMap(
            key: ImagesKeys.mapWithMarkers,
            mapType: MapType.hybrid,
            initialCameraPosition: state.initialLocation,
            markers: state.markers,
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
          );
        },
      ),
    );
  }
}
