import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:gallery_lab/images_bloc/image.dart' as app_image;

class ImageItem extends StatelessWidget {
  final GestureTapCallback onTap;
  final app_image.Image image;

  const ImageItem({
    required Key key,
    required this.onTap,
    required this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Center(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: CachedNetworkImage(
            imageUrl: image.url,
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.width,
            fit: BoxFit.fitWidth,
            errorWidget: (context, url, error) => const Icon(Icons.error),
          ),
        ),
      ),
      onTap: onTap,
    );
  }
}
