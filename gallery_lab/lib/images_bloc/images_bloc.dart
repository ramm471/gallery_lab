import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:gallery_lab/images_bloc/image.dart';
import 'package:gallery_lab/images_bloc/images_event.dart';
import 'package:gallery_lab/images_bloc/images_state.dart';
import 'package:gallery_lab/repository/images_repository.dart';

class ImagesBloc extends Bloc<ImagesEvent, ImagesState> {
  final ImagesRepository imagesRepository;

  ImagesBloc({required this.imagesRepository}) : super(ImagesLoadInProgress());

  @override
  Stream<ImagesState> mapEventToState(ImagesEvent event) async* {
    if (event is ImagesLoaded) {
      yield* _mapImagesLoadedToState();
    } else if (event is ImageAdded) {
      yield* _mapImageAddedToState(event);
    }
  }

  Stream<ImagesState> _mapImagesLoadedToState() async* {
    try {
      final images = await imagesRepository.loadImages();
      yield ImagesLoadSuccess(
        images.map(Image.fromEntity).toList(),
      );
    } catch (e) {
      yield ImagesLoadFailure(e);
    }
  }

  Stream<ImagesState> _mapImageAddedToState(ImageAdded event) async* {
    if (state is ImagesLoadSuccess) {
      final List<Image> updatedImages = List.from((state as ImagesLoadSuccess).images)..addAll(event.images);
      yield ImagesLoadSuccess(updatedImages);
      _saveImage(event.images)
          .onError((error, stackTrace) => print("_saveImages error $error; stackTrace $stackTrace"));
    }
  }

  Future _saveImage(List<Image> images) {
    return imagesRepository.saveImage(images.map((e) => e.toEntity()).toList());
  }
}
