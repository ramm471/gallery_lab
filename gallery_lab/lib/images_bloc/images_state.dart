import 'package:equatable/equatable.dart';
import 'package:gallery_lab/images_bloc/image.dart';

abstract class ImagesState extends Equatable {
  const ImagesState();

  @override
  List<Object> get props => [];
}

class ImagesLoadInProgress extends ImagesState {}

class ImagesLoadSuccess extends ImagesState {
  final List<Image> images;

  const ImagesLoadSuccess([this.images = const []]);

  @override
  List<Object> get props => [images];

  @override
  String toString() => 'ImagesLoadSuccess { images_bloc: $images }';
}

class ImagesLoadFailure extends ImagesState {
  final Object error;

  const ImagesLoadFailure(this.error);

  @override
  List<Object> get props => [error];

  @override
  String toString() => 'ImagesLoadFailure { error: $error }';
}
