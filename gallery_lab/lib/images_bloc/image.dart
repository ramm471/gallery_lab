import 'package:equatable/equatable.dart';
import 'package:gallery_lab/repository/image_entity.dart';
import 'package:uuid/uuid.dart';

class Image extends Equatable {
  final String id;
  final String note;
  final String url;
  final double latitude;
  final double longitude;

  Image({String? id, String? note, String? url, double? latitude, double? longitude})
      : id = id ?? const Uuid().v4(),
        note = note ?? "",
        url = url ?? "",
        latitude = latitude ?? 0,
        longitude = longitude ?? 0;

  Image copyWith({String? id, String? note, String? url, double? latitude, double? longitude}) {
    return Image(
      id: id ?? this.id,
      note: note ?? this.note,
      url: url ?? this.url,
      latitude: latitude ?? this.latitude,
      longitude: longitude ?? this.longitude,
    );
  }

  @override
  List<Object> get props => [id, note, url, latitude, longitude];

  @override
  String toString() {
    return 'Image { id: $id, note: $note, url: $url, latitude: $latitude, longitude: $longitude}';
  }

  ImageEntity toEntity() {
    return ImageEntity(id, note, url, latitude, longitude);
  }

  static Image fromEntity(ImageEntity entity) {
    return Image(
      id: entity.id,
      note: entity.note,
      url: entity.url,
      latitude: entity.latitude,
      longitude: entity.longitude,
    );
  }
}
