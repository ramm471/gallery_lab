import 'package:equatable/equatable.dart';
import 'package:gallery_lab/images_bloc/image.dart';

abstract class ImagesEvent extends Equatable {
  const ImagesEvent();

  @override
  List<Object> get props => [];
}

class ImagesLoaded extends ImagesEvent {}

class ImageAdded extends ImagesEvent {
  final List<Image> images;

  const ImageAdded(this.images);

  @override
  List<Object> get props => [images];

  @override
  String toString() => 'ImageAdded { images: $images }';
}
